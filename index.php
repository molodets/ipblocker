<?php

require_once 'includes/main.php';

//if(isset($_POST['login'])) {}

?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>i.b</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/dopstyle.css" rel="stylesheet" media="screen">

    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
    <div class="container">
        <div class="row">
            <div class="col-md-12 top-menu">
                <ul>
                    <li><a href="#">MyIpBlocker</a></li>
                    <li><a href="#">How it works</a></li>
                    <li><a href="#">Contact us</a></li>
                    <li><a href="register.php">Register</a></li>
                    <li>
                        <form method="POST">
                            <input value="" maxlength="25" type="text" name="login" 
                                           placeholder="Your Login Here" class="text" required />
                            
                            <input maxlength="25" type="password" name="password" 
                                   placeholder="Your Password Here" class="text" required />
                            <button>Send</button>
                            <br>
                            <a href="forgottenPassword.php">Forgotten Password?</a>
                            <span id="error"></span>
                            
                        </form>
                    </li>
                </ul>
            </div>
                <div class="col-md-12">
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                      <!-- Маркеры слайдов -->
                      <ol class="carousel-indicators">
                        <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                        <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                      </ol>

                      <!-- Содержимое слайдов -->
                      <div class="carousel-inner">
                        <div class="item active">
                          <img src="images/1.jpg" alt="...">
                          <div class="carousel-caption">
                            <h3>Россия (Москва)</h3>
                            <p>Где это видано, чтобы люди в Москве без прописки проживали.</p>
                          </div>
                        </div>

                        <div class="item">
                          <img src="images/2.jpg" alt="...">
                          <div class="carousel-caption">
                            <h3>Осень</h3>
                            <p>Осень — это вторая весна, когда каждый лист — цветок.</p>
                          </div>
                        </div>

                        <div class="item">
                          <img src="images/3.jpg" alt="...">
                          <div class="carousel-caption">
                            <h3>Дождь</h3>
                            <p>Осень опять идут дожди...</p>
                          </div>
                        </div>
                      </div>

                      <!-- Controls -->
                      <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                      </a>
                      <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                      </a>
                    </div>

                </div>
            <div class="col-md-12 top-material"></div>
            <div class="col-md-3 left-sidebar"></div>
            <div class="col-md-9 content"></div>
        </div>
    </div>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

  </body>
</html>

<?php
/*ini_set('display_errors', 1);
error_reporting(E_ALL);

session_start();
$link = mysqli_connect('localhost', 'root', '1', 'IpBlocker');

if(isset($_POST['login'])) {
    $login = mysqli_real_escape_string($link, $_POST['login']);
    $query = mysqli_query($link, "SELECT id, pass, activated FROM users "
            . "WHERE login='$login' LIMIT 1");
    //echo "<hr>";
    //echo $query;
    //echo "<hr";

    if ($query) {
        $data = mysqli_fetch_assoc($query);
        if ($data['pass'] === sha1(md5($_POST['password']).md5($login)."7")) {
            if ($data['activated']){
                $_SESSION['userid'] = $data['id'];
                header('Location: protected.php');
                //exit;
            }
            else {
                echo "You need to VALIDATE your account clicking on the link sent to your email.";
            }
        }
        else { 
            ?>
            <script>
                $("#error").text("Wrong Login or Password.");
            </script>
            <?php
        }
    }

    else {
        print "Wrong Login or Password1";
    }

}