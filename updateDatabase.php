<a href="index.php">Index</a>

<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);

//require_once 'vendors/3rd party/libs/idiorm.php';

$dsn = 'mysql:host=localhost;dbname=IpBlocker';
$username = 'root';
$password = '1';
$db = new PDO($dsn, $username, $password);

function download(){
    $curl = curl_init('http://geolite.maxmind.com/download/geoip/database/GeoLiteCity_CSV/GeoLiteCity-latest.zip');
    $fp =fopen('test.zip','w');
    curl_setopt($curl, CURLOPT_FILE, $fp);
    curl_setopt($curl, CURLOPT_HEADER, 0);
    curl_exec($curl);
    curl_close($curl);
    fclose($fp);
}

function unzip(){
    $zip = new ZipArchive;
    $file = realpath("test.zip");
    $res = $zip->open($file);
    global $path;
    $path[] = $zip->statIndex (0);
    $path[] = $zip->statIndex (1);
    if ($res === TRUE) {
        $zip->extractTo('unzip');
        $zip->close();
    } else {
        echo 'failed, code:' . $res;
    }
}

function import($filename, $table, $db){
    $q_import =
        "LOAD DATA INFILE '". 
        $_SERVER['DOCUMENT_ROOT']."/unzip/".$filename."' INTO TABLE ".$table." 
        FIELDS TERMINATED BY ',' 
        ENCLOSED BY '\"' 
        LINES TERMINATED BY '\\n' 
        IGNORE 2 LINES"
        ;
    $db->query($q_import);
    //echo $q_import;
}

function backup($db){
    $db->query("CREATE TABLE IF NOT EXISTS Location_backup (
            locId INT(6),
            country VARCHAR(4),
            region VARCHAR(4),
            city VARCHAR(50),
            postalCode VARCHAR(10),
            latitude FLOAT,
            longitude FLOAT,
            metroCode INT(3),
            areaCode INT(3),
            PRIMARY KEY (locId)
          );");
    $numRows = $db->query("SELECT COUNT(*) FROM Location;")->fetch();
    if ($numRows[0]>400000){
        $db->query("TRUNCATE TABLE Location_backup;");
        $db->query("INSERT Location_backup SELECT * FROM Location;");
        $db->query("TRUNCATE TABLE Location;");
    }

    $db->query("CREATE TABLE IF NOT EXISTS Blocks_backup (
             startIpNum BIGINT(12),
             endIpNum BIGINT(12),
             locId INT(6),
             PRIMARY KEY (startIpNum)
           );");
    $numRows = $db->query("SELECT COUNT(*) FROM Blocks;")->fetch();
    if ($numRows[0]>1500000){
        $db->query("TRUNCATE TABLE Blocks_backup;");
        $db->query("INSERT Blocks_backup SELECT * FROM Blocks;");
        $db->query("TRUNCATE TABLE Blocks;");
    }
}

//download();
unzip();
//backup($db);
import($path[0]['name'], "Blocks", $db);
//import($path[1]['name'], "Location", $db);