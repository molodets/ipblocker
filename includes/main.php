<?php

/**
 * Include the libraries
 */

ini_set('display_errors', 1);
error_reporting(E_ALL);

require_once __DIR__."/idiorm.php";                     //Зачем ДИР? И так же ищет в своей папке
require_once __DIR__."/User.class.php";
require_once __DIR__."/functions.php";

/**
 * Configure Idiorm
 */

$db_host = 'localhost';
$db_name = 'IpBlocker';
$db_user = 'root';
$db_pass = '1';

ORM::configure("mysql:host=$db_host;dbname=$db_name");
ORM::configure("username", $db_user);
ORM::configure("password", $db_pass);

// Set the database connection to UTF-8
//ORM::configure('driver_options', array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));

/**
 * Configure the session
 */

session_name('tzreg');

// Uncomment to keep people logged in for a week
// session_set_cookie_params(60 * 60 * 24 * 7);

session_start();