<HTML>
<HEAD><TITLE>
IP2Location IP-COUNTRY-REGION-CITY Database Demo in PHP Hypertext Preprocessor
</TITLE>
	<STYLE> 
		TABLE,BODY,P{ color:Black; font-family:Verdana,Geneva,Arial,Helvetica,sans-serif; font-size:12px; }
		H1{ color:Black; font-family:Verdana,Geneva,Arial,Helvetica,sans-serif; font-size:16px; }
		A.blue,A.blue:VISITED,A.blue:LINK{ text-decoration:none;font-weight:bolder; color:Blue; }
	</STYLE>
</HEAD>
<BODY bgcolor="#FFFFFF">
<center>
<h1><b><u>IP2Location?Demo - Lookup Country, Region & City by IP Address</u></h1></b>
Enter a dotted IP address (xxx.xxx.xxx.xxx) between <b>12.4.254.0</b> and <b>12.5.78.255</b>.<br>
<?php
//---------------------------------------------------------------------------
// Title        : Lookup Country, Region & City by IP Address
// Description  : This script lookup the country, region & city by IP address.
//                Things that we can do with this script.
//                	1. Display native language and currency 
//                	2. Redirect based on country 
//                	3. Digital rights management 
//                	4. Prevent password sharing and abuse of service 
//                	5. Reduce credit card fraud 
//                	6. Web log stats and analysis 
//                	7. Auto-selection of country on forms 
//                	8. Filter access from countries you do not do business with 
//                	9. Geo-targeting for increased sales and click-thrus 
//                	10. And much, much more! 
// Requirements : PHP 4+ and MySQL
// Installation : 1. Copy ipcountryregioncity.php into a web directory.
//                2. Configure the web directory with IIS.
//                3. Setup SQL Database in MySQL. See next section.
//                4. Browse ipcountryregioncity.php using http protocol.
//                   example: http://localhost/ip2location/ipcountryregioncity.php
//                5. Enter an IP address range between 12.4.254.0 and 12.5.78.255 and click submit.
// Setup Database (MySQL) :
//                1. Open command prompt.
//                2. Set path to MySQL's bin path. (example: path = %path%; "C:\Program Files\MySQL\MySQL Server 4.1\bin").
//                3. Enter 'MySql -u <UserID> -p<Password> -h <localhost> < MySql-Script.sql'
//
// Author       : IP2Location.com
// URL          : http://www.ip2location.com
//
// Copyright (c) 2002-2008 IP2Location.com
//---------------------------------------------------------------------------


	echo	"  <form action=\"" . $_SERVER['PHP_SELF'] . "\" method=\"POST\">\n";
	echo	"    <textarea name=\"ipaddress\" cols=15 rows=6></textarea><br>\n";
	echo	"    <input type=\"submit\" name=\"submit\" value=\"submit\">\n";
	echo	"  </form>\n";
	
	if (isset($HTTP_POST_VARS["ipaddress"])) 
	{
		// get the IP address from the form 
		$ipaddress = $HTTP_POST_VARS["ipaddress"];
		$ipaddress = str_replace(" ","\n", $ipaddress);
		$arripaddress = split("\n", $ipaddress);
		if (count($arripaddress) > 0)
		{
			echo	"<p>";
			echo	"<h1><u>Lookup Result</u></h1>";
			// display header
			echo	"<table border = 1>";
			echo	"<tr>";
			echo	"<td align=center>IP Address</td>";
			echo	"<td align=center>Country Name (Short)</td>";
			echo	"<td align=center>Country Name (Long)</td>";
			echo	"<td align=center>Region Name</td>";
			echo	"<td align=center>City Name</td>";
			echo	"</tr>";
			for ($i = 0; $i <= (count($arripaddress)-1); $i++)
			{
				if ($arripaddress[$i] != "")
				{
					$ipno = Dot2LongIP($arripaddress[$i]);
					// check if the IP address is supported in demo version
					if (($ipno < 201653760) || ($ipno > 201674495)) 
					{
						echo	"IP address " . $arripaddress[$i] . " is not supported in demo version.<br>\n";
						echo "<tr>";
						echo "<td align=center>" . $arripaddress[$i] . "</td>";
						echo "<td align=center>-</td>";
						echo "<td align=center>-</td>";
						echo "<td align=center>-</td>";
						echo "<td align=center>-</td>";
						echo "</tr>";
					} 
					else 
					{
						$dbhost = 'localhost';
						$dbuser = 'root';
						$dbpass = '12345';
						$dbname = 'ip2location';
						$query  = "SELECT * FROM IPCountryRegionCity WHERE " . $ipno . " <= ipTO LIMIT 1";
						$conn = mysql_connect($dbhost, $dbuser, $dbpass) or die ('Error connecting to mysql');
						mysql_select_db($dbname);
						$result = mysql_query($query);
						// display results
						if ($result != null)
						{
	 						while($row = mysql_fetch_array($result, MYSQL_ASSOC))
							{
								echo "<tr>";
								echo "<td align=center>" . $arripaddress[$i] . "</td>";
								echo "<td align=center>{$row['countrySHORT']}</td>";
								echo "<td align=center>{$row['countryLONG']}</td>";
								echo "<td align=center>{$row['ipREGION']}</td>";
								echo "<td align=center>{$row['ipCITY']}</td>";
								echo "</tr>";
							}
						} 
						else 
						{
							echo "<tr>";
							echo "<td align=center>" . $arripaddress[$i] . "</td>";
							echo "<td align=center>-</td>";
							echo "<td align=center>-</td>";
							echo "<td align=center>-</td>";
							echo "<td align=center>-</td>";
							echo "</tr>";
						}
	 					mysql_close($conn); 
					}
				}
			}
			echo	"</table>";
			echo	"</p>";
		}
		else
		{
			echo "Please enter IP address.";
		}
	}
	// Convert dotted IP address into IP number in long
	function Dot2LongIP ($IPaddr)
	{
		if ($IPaddr == "") 
		{
			return 0;
		} 
		else 
		{
			$ips = split ("\.", "$IPaddr");
			return ($ips[3] + $ips[2] * 256 + $ips[1] * 256 * 256 + $ips[0] * 256 * 256 * 256);
		}
	}
?>
		<BR>
		[<A href=""http://www.ip2location.com"">Click here to subscribe full database</A>]
		<BR>
		<BR>
		Copyright (c) 2002-2008 by <a href="http://www.ip2location.com">IP2Location.com</a>
		<P>
		<a href="http://www.ip2location.com/"><img src="http://www.ip2location.com/images/ip2location468x60_0.gif" border=""0""></a>
		</P>
		</center>
	</BODY>
</HTML>
